/**
 * Executes the StackBlitz SDK's embedProjectId method
 */
let process = true;
(function ($, Drupal, once) {
  Drupal.behaviors.waveformBehavior = {
    attach: function (context, settings) {
      once('stackBlitzEmbed', 'body', context).forEach((element) => {
        if (process == true && window.StackBlitzSDK) {
          process = false;
          let keys = Object.keys(settings.stackblitz);
          keys.map(key => {
          window.StackBlitzSDK.embedProjectId('stackblitz-embed-'+key, settings.stackblitz[key].field_stackblitz_project_id, {
            forceEmbedLayout: settings.stackblitz[key].force_embed_layout,
            hideNavigation: settings.stackblitz[key].hide_navigation,
            hideExplorer: settings.stackblitz[key].hide_explorer,
            hideDevTools: settings.stackblitz[key].hide_dev_tools,
            height: settings.stackblitz[key].embed_height,
            view: settings.stackblitz[key].embed_default_view,
            clickToLoad: settings.stackblitz[key].click_to_load
          });
        });
        }
      });
    }
  }
})(jQuery, Drupal, once);
