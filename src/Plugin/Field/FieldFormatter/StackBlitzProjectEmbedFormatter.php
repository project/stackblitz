<?php

namespace Drupal\stackblitz\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'StackBlitz Project' formatter.
 *
 * @FieldFormatter(
 *   id = "stackblitz_project_embed",
 *   label = @Translation("StackBlitz Project"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class StackBlitzProjectEmbedFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'click_to_load' => 1,
      'force_embed_layout' => 0,
      'hide_navigation' => 0,
      'hide_explorer' => 0,
      'hide_dev_tools' => 0,
      'embed_default_view' => 'both',
      'embed_height' => 320,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements['click_to_load'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Click To Load'),
      '#description' => $this->t('Load editor only when clicked'),
      '#default_value' => $this->getSetting('click_to_load'),
    ];
    $elements['force_embed_layout'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Force Embed Layout'),
      '#description' => $this->t('Force embed view regardless of screen size'),
      '#default_value' => $this->getSetting('force_embed_layout'),
    ];
    $elements['hide_navigation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide Navigation'),
      '#description' => $this->t('Hide the browser navigation UI'),
      '#default_value' => $this->getSetting('hide_navigation'),
    ];
    $elements['hide_explorer'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide Explorer'),
      '#description' => $this->t('Hide file explorer pane in embed view'),
      '#default_value' => $this->getSetting('hide_explorer'),
    ];
    $elements['hide_dev_tools'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide DevTools'),
      '#description' => $this->t('Hide the debugging console in the editor preview'),
      '#default_value' => $this->getSetting('hide_dev_tools'),
    ];

    $viewoptions = [];
    $viewoptions['editor'] = 'Editor';
    $viewoptions['preview'] = 'Preview';
    $elements['embed_default_view'] = [
      '#type' => 'select',
      '#title' => $this
        ->t('Default View'),
      '#description' => $this->t('Which view to open by default'),
      '#options' => $viewoptions,
      '#default_value' => $this->getSetting('embed_default_view'),
    ];
    $elements['embed_height'] = [
      '#type' => 'number',
      '#title' => $this->t('Height'),
      '#default_value' => $this->getSetting('embed_height'),
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    if ($this->getSetting('embed_default_view') == 'preview') {
      $defaultview = $this->t('Preview');
    }
    else {
      $defaultview = $this->t('Editor');
    }
    if ($this->getSetting('click_to_load') == 1) {
      $summary[] = $this->t('Click to Load') . " " . $defaultview;
    }
    else {
      $summary[] = $defaultview;
    }
    $show = '';
    if ($this->getSetting('hide_navigation') == 0) {
      $show .= $this->t('Navigation') . " ";
    }
    if ($this->getSetting('hide_dev_tools') == 0) {
      $show .= $this->t('DevTools') . " ";
    }
    if ($this->getSetting('hide_explorer') == 0) {
      $show .= $this->t('Explorer') . " ";
    }
    if (strlen($show) > 0) {
      $summary[] = $this->t('Showing') . " " . $show;
    }
    if ($this->getSetting('force_embed_layout') == 1) {
      $summary[] = $this->t('Force Embed Layout');
    }
    $summary[] = $this->t('Height: @embed_height', ['@embed_height' => $this->getSetting('embed_height')]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    foreach ($items as $delta => $item) {
      $element[$delta] = [
        '#item' => $items->getEntity()->id(),
        '#theme' => 'stackblitz_embed',
        '#attached' => [
          'library' => [
            'stackblitz/stackblitz',
          ],
        ],
      ];
      $element[$delta]['#attached']['drupalSettings']['stackblitz'][$items->getEntity()->id()] = [
        'field_stackblitz_project_id' => $item->value,
        'embed_height' => $this->getSetting('embed_height'),
        'embed_default_view' => $this->getSetting('embed_default_view'),
        'hide_navigation' => $this->getSetting('hide_navigation'),
        'hide_dev_tools' => $this->getSetting('hide_dev_tools'),
        'hide_explorer' => $this->getSetting('hide_explorer'),
        'force_embed_layout' => $this->getSetting('force_embed_layout'),
        'click_to_load' => $this->getSetting('click_to_load'),
      ];
    }
    return $element;
  }

}
