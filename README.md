StackBlitz
=======================

Project site: http://drupal.org/project/stackblitz

Code: https://drupal.org/project/stackblitz

Issues: https://drupal.org/project/issues/stackblitz

What Is This?
-------------
Embed a StackBlitz Project using the project's ID.

This module provides a field formatter that can be set as the display
format for a text field containing a StackBlitz project's ID. The display
is configurable per the field formatter settings, so Views can display
multiple projects with the "click to run" setting enabled, whereas the
full content display can run the project and display the editor alongside
the preview window, etc..

The module also provides a StackBlitz Project content type, a text field
setup to display StackBlitz Projects, and view that lists them in a grid.
The content type, field and view can be deleted. The StackBlitz Project
field formatter can be applied to any text field that contains valid project
IDs.
